// Scene, Camera, Renderer
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

// Lighting
const ambientLight = new THREE.AmbientLight(0x404040); // soft white light
scene.add(ambientLight);
const sunlight = new THREE.PointLight(0xffffff, 1.5, 1000);
sunlight.position.set(0, 0, 0);
scene.add(sunlight);

// --- Planet Data (You provide this) ---
const planetData = {
    "Sun": {
        radius: 20,
        texture: 'textures/sunmap.jpg',
        rotationSpeed: 0.004
    },
    "Mercury": {
        radius: 1, 
        texture: 'textures/mercurymap.jpg',
        orbitRadius: 30, 
        orbitSpeed: 0.04,
        rotationSpeed: 0.001 
    },
    "Venus": {
        radius: 2.5, 
        texture: 'textures/venusmap.jpg',
        orbitRadius: 35, 
        orbitSpeed: 0.03,
        rotationSpeed: 0.0005
    },
    "Earth": {
        radius: 3, 
        texture: 'textures/earthmap.jpg',
        orbitRadius: 40, 
        orbitSpeed: 0.02,
        rotationSpeed: 0.01 
    },
    "Mars": {
        radius: 1.5, 
        texture: 'textures/marsmap.jpg',
        orbitRadius: 45,
        orbitSpeed: 0.015,
        rotationSpeed: 0.009 
    },
    "Jupiter": {
        radius: 10, 
        texture: 'textures/jupitermap.jpg',
        orbitRadius: 60, 
        orbitSpeed: 0.01,
        rotationSpeed: 0.013
    },
    "Saturn": {
        radius: 8, 
        texture: 'textures/saturnmap.jpg',
        orbitRadius: 75, 
        orbitSpeed: 0.008,
        rotationSpeed: 0.012
    },
    "Uranus": {
        radius: 6, 
        texture: 'textures/uranusmap.jpg',
        orbitRadius: 90, 
        orbitSpeed: 0.005,
        rotationSpeed: 0.007 
    }
};

// Create Planets
function createPlanet(planetName, data) {
    const texture = new THREE.TextureLoader().load(data.texture);
    const bumpTexture = new THREE.TextureLoader().load(data.texture.replace('map', 'bump')); // e.g., 'earthmap.jpg' -> 'earthbump.jpg'
    const geometry = new THREE.SphereGeometry(data.radius, 32, 32);
    const material = new THREE.MeshStandardMaterial({ 
        map: texture,
        bumpMap: bumpTexture,
        bumpScale: 0.05,
        rotationSpeed: data.rotationSpeed || 0.00
    });
    const planet = new THREE.Mesh(geometry, material);

    // Orbit (if applicable)
    if (data.orbitRadius) {
        const orbit = new THREE.Object3D();
        orbit.position.x = data.orbitRadius;
        orbit.add(planet);
        scene.add(orbit);
        planet.orbit = orbit;
        planet.orbitSpeed = data.orbitSpeed;
    } else {
        scene.add(planet);
    }

    return planet;
}

const planets = {};
for (const planetName in planetData) {
    planets[planetName] = createPlanet(planetName, planetData[planetName]);
}

camera.position.z = 100; // Initial camera position

// Animation Loop
function animate() {
    requestAnimationFrame(animate);

    // Planet Rotation
    for (const planetName in planets) {
        // planets[planetName].rotation.y += 0.005; // Adjust rotation speed
        // Orbit Animation
        if (planets[planetName].orbit) {
            planets[planetName].orbit.rotation.y += planets[planetName].orbitSpeed;
        }
    }

    renderer.render(scene, camera);
}

animate();
